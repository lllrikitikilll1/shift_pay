import uvicorn
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from src.models import Base
from src.crud import create_payment, create_user, get_current_user, get_payment
from src.database import engine, get_db
from src.gen_token import create_access_token, verify_access_token
from src.utils import hash_password, verify_password
from src.schemas import UserLoginSchema, TokenSchemas, PaymentSchema

Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.post("/token/")
def token(user: UserLoginSchema, db: Session = Depends(get_db)):
    db_user = get_current_user(user, db)
    if not db_user:
        user.password = hash_password(user.password)
        db_user = create_user(user, db)
        create_payment(db_user, db)
    else:
        if not verify_password(user.password, db_user.password):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail="Неверный логин или пароль")

    token = create_access_token(data={"user_id": db_user.id})
    return token


@app.post('/payment/')
def payment(token: TokenSchemas, db: Session = Depends(get_db)) -> PaymentSchema:
    payload = verify_access_token(token=token.token)
    payment = get_payment(user_id=payload.id, db=db)
    return payment


if __name__ == "__main__":
    uvicorn.run(app)
