import datetime

from sqlalchemy.orm import Session

from src import models, schemas


def get_current_user(user: schemas.UserLoginSchema, db: Session):
    user = db.query(models.User).filter(models.User.login == user.login).first()
    return user


def create_user(user: schemas.UserLoginSchema, db: Session):
    db_user = models.User(login=user.login, password=user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_payment(user: schemas.UserSchema, db: Session) -> None:
    db_payment = models.Payment(user_id=user.id,
                                salary=10_000,
                                date_increase=datetime.datetime.utcnow() + datetime.timedelta(days=360))
    db.add(db_payment)
    db.commit()
    return


def get_payment(user_id: int, db: Session):
    payment = db.query(models.Payment).filter(models.Payment.user_id == user_id).first()
    return payment
