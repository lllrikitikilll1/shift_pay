import datetime

from pydantic import BaseModel


class TokenSchemas(BaseModel):
    token: str


class UserLoginSchema(BaseModel):
    """Валидация данных для входа"""
    login: str
    password: str


class UserSchema(UserLoginSchema):
    """Валидация данных пользователя"""
    id: int


class TokenDataValid(BaseModel):
    """Валидация расшифрованного токена"""
    id: int


class PaymentSchema(BaseModel):
    """Валидация данных о зарплате"""
    user_id: int
    salary: int
    date_increase: datetime.datetime
