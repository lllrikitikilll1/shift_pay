import datetime

from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column

from src.database import Base


class User(Base):
    """Модель таблицы пользователей"""
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str] = mapped_column(String(30), unique=True)
    password: Mapped[str] = mapped_column(String(30))


class Payment(Base):
    """Модель таблицы данных о зарплате"""
    __tablename__ = "Payment"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id = mapped_column(ForeignKey('user.id'))
    salary: Mapped[int] = mapped_column()
    date_increase: Mapped[datetime.datetime]
